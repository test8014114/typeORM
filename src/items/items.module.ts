import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Item } from "./entities/item.entity";
import { Comment } from "./entities/comment.entity";
import { Listing } from "./entities/listing.entity";
import { ItemsController } from "./controllers/items.controller";
import { ItemsService } from "./services/items.service";

@Module({
  imports: [TypeOrmModule.forFeature([Item,Comment,Listing])],
  controllers: [ItemsController],
  providers: [ItemsService],

})

export class ItemsModule {}