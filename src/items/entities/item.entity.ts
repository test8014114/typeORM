import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Listing } from "./listing.entity";
import { Comment } from './comment.entity';
import { Tags } from "./tag.entity";

@Entity()
export class Item {

    @PrimaryGeneratedColumn('uuid')
    id : string;

    @Column()
    name: string;

    @Column({default:true})
    public:boolean;

    @OneToOne(()=>Listing,{cascade:true})
    @JoinColumn()
    listing: Listing;

    @OneToMany(()=>Comment, (comment)=> comment.item,{cascade: true})
    comments: Comment[];

    @ManyToMany(()=>Tags, {cascade: true})
    @JoinTable()
    tags:Tags[];
}
