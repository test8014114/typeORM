
import { IsNotEmpty } from "class-validator";
import { CreateListingDto } from "./create-listing.dto";
import { CreateTagDto } from "./create-tag.dto";


export class CreateItemDto {
    @IsNotEmpty()
    name: string;

    public: boolean;

    listing: CreateListingDto;

    tag : CreateTagDto[];

}
