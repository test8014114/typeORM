import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Item } from './item.entity';

@Entity()
export  class Comment{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    content:  string;
    
    @ManyToOne(()=>Item, (item)=>item.comments)
    item: Item;
     
}