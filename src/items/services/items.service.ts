import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateItemDto } from '../dto/create-item.dto';
import { UpdateItemDto } from '../dto/update-item.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Item } from '../entities/item.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ItemsService {
  constructor(@InjectRepository(Item) private itemRepository: Repository<Item>) {}


  
  async create(createItemDto: CreateItemDto) {
    try {
      const newItem = this.itemRepository.create({ ...createItemDto, comments: [] });
      return await this.itemRepository.save(newItem);
    } catch (error) {
      throw new Error(`Failed to create item: ${error.message}`);
    }
  }

  async findAll() {
    try {
      return await this.itemRepository.find();
    } catch (error) {
      throw new Error(`Failed to find items: ${error.message}`);
    }
  }



  async findOne(id: string) {
    try {
      const item = await this.itemRepository.findOne({
        where: { id },
        relations: ['listing', 'comments', 'tags'],
      });
      if (!item) {
        throw new NotFoundException(`Item with id ${id} dddd`);
      }
      return item;
    } catch (error) {
      throw new Error(`Failed to find item: ${error.message}`);
    }
  }



  async update(id: string, updateItemDto: UpdateItemDto) {
    try {
      const item = await this.findOne(id);
      return await this.itemRepository.save({ ...item, ...updateItemDto });
    } catch (error) {
      throw new Error(`Failed to update item: ${error.message}`);
    }
  }


  async remove(id: string) {
    try {
      const item = await this.findOne(id);
      return await this.itemRepository.remove(item);
    } catch (error) {
      throw new Error(`Failed to remove item: ${error.message}`);
    }
  }
}
