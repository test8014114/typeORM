import { MigrationInterface, QueryRunner } from "typeorm";

export class NewMigrations1712232183445 implements MigrationInterface {
    name = 'NewMigrations1712232183445'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "tags" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "content" character varying NOT NULL, CONSTRAINT "PK_e7dc17249a1148a1970748eda99" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "listing" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "description" character varying NOT NULL, "rating" integer NOT NULL DEFAULT '4', CONSTRAINT "PK_381d45ebb8692362c156d6b87d7" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "item" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "public" boolean NOT NULL DEFAULT true, "listingId" uuid, CONSTRAINT "REL_d989034b34992567ecb91ea60a" UNIQUE ("listingId"), CONSTRAINT "PK_d3c0c71f23e7adcf952a1d13423" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "comment" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "content" character varying NOT NULL, "itemId" uuid, CONSTRAINT "PK_0b0e4bbc8415ec426f87f3a88e2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "item_tags_tags" ("itemId" uuid NOT NULL, "tagsId" uuid NOT NULL, CONSTRAINT "PK_e829a806ad31894847307ab9c55" PRIMARY KEY ("itemId", "tagsId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f75406f3a4bee0f4a67f462cca" ON "item_tags_tags" ("itemId") `);
        await queryRunner.query(`CREATE INDEX "IDX_fa118421c03291697ce5df48fe" ON "item_tags_tags" ("tagsId") `);
        await queryRunner.query(`ALTER TABLE "item" ADD CONSTRAINT "FK_d989034b34992567ecb91ea60a5" FOREIGN KEY ("listingId") REFERENCES "listing"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_d7846a91e6eb1ddcef861577e02" FOREIGN KEY ("itemId") REFERENCES "item"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "item_tags_tags" ADD CONSTRAINT "FK_f75406f3a4bee0f4a67f462cca1" FOREIGN KEY ("itemId") REFERENCES "item"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "item_tags_tags" ADD CONSTRAINT "FK_fa118421c03291697ce5df48fe1" FOREIGN KEY ("tagsId") REFERENCES "tags"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "item_tags_tags" DROP CONSTRAINT "FK_fa118421c03291697ce5df48fe1"`);
        await queryRunner.query(`ALTER TABLE "item_tags_tags" DROP CONSTRAINT "FK_f75406f3a4bee0f4a67f462cca1"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_d7846a91e6eb1ddcef861577e02"`);
        await queryRunner.query(`ALTER TABLE "item" DROP CONSTRAINT "FK_d989034b34992567ecb91ea60a5"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fa118421c03291697ce5df48fe"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_f75406f3a4bee0f4a67f462cca"`);
        await queryRunner.query(`DROP TABLE "item_tags_tags"`);
        await queryRunner.query(`DROP TABLE "comment"`);
        await queryRunner.query(`DROP TABLE "item"`);
        await queryRunner.query(`DROP TABLE "listing"`);
        await queryRunner.query(`DROP TABLE "tags"`);
    }

}
