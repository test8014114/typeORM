import { defaultMaxListeners } from "events";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Listing{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    description:  string;
    
    @Column({default : 4})
    rating: number;

}