
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Tags{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    content: string;

}