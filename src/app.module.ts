import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import{ dataSourceOptions } from 'db/data-source';
import { ItemsModule } from './items/items.module';

@Module({
  imports: [TypeOrmModule.forRoot(dataSourceOptions), ItemsModule,],
})
export class AppModule {}
