
import { CreateCommentDto } from './create-comment.dto';
import { UpdateListingDto } from './update-listing.dto';


export class UpdateItemDto {
    public: boolean;
    listing: UpdateListingDto;
    comments: CreateCommentDto[];
}
